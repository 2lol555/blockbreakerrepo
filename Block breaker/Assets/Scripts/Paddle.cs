﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour {
	void Start(){
		ball = GameObject.FindObjectOfType<Ball>();
	
	}
	
	public bool autoPlay = false;
	
	private Ball ball;
		
	void Update () {
		if(!autoPlay){
			MoveWithMouse();
		} else{
			AutoPlay();	
		}
	}
	void AutoPlay(){
		Vector3 paddlePos = new Vector3(0.5f, this.transform.position.y, 0f);
		Vector3 ballPos = ball.transform.position;
		paddlePos.x = Mathf.Clamp(ballPos.x , 1F, 15F);
		this.transform.position = paddlePos;
	}
	void MoveWithMouse(){
		Vector3 paddlePos = new Vector3(0.5f, this.transform.position.y, 0f);
		paddlePos.x = Mathf.Clamp(Input.mousePosition.x / Screen.width * 16, 1F, 15F);
		this.transform.position = paddlePos;
	}
}
