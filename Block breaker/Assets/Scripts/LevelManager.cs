﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public void LoadLevel(string name){
		Debug.Log("Load for level " + name + " requested");
		Brick.breakableCount = 0;
		Application.LoadLevel(name);
	}
	public void LoadNextLevel(){
		Brick.breakableCount = 0;
		Application.LoadLevel(Application.loadedLevel + 1);

	}
	
	public void Quit_Request(){
		Debug.Log("Quit requested");
		Application.Quit();
	}
	
	public void brickDestroyed(){
		if(Brick.breakableCount <= 0){
			Brick.breakableCount = 0;
			LoadNextLevel();

		}
	}
}
