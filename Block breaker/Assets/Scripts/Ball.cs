﻿using UnityEngine;
using System.Collections;



public class Ball : MonoBehaviour {
	//A public Paddle with our name for it in the script as "paddle"
	private Paddle paddle;
	private bool hasStarted = false;
	private Vector3 paddleToBallVector;
	// Use this for initialization
	void Start () {
	paddle = GameObject.FindObjectOfType<Paddle>();
		paddleToBallVector = this.transform.position - paddle.transform.position;
		print(paddleToBallVector);
	}
	
	// Update is called once per frame
	void Update() {
		if(!hasStarted){
		this.transform.position = paddle.transform.position + paddleToBallVector;
		
			if(Input.GetMouseButtonDown(0)){
				this.GetComponent<Rigidbody2D>().velocity = new Vector2(5f,10f);
				hasStarted = true;
			}
		
		}

	}
	void OnCollisionEnter2D(Collision2D collision){
		//generating random velocity to add to ball to stop boring game loops
		Vector2 tweak = new Vector2(Random.Range(0f, 0.2f), Random.Range(0f, 0.2f));
		if(hasStarted){GetComponent<AudioSource>().Play();}
		GetComponent<Rigidbody2D>().velocity += tweak;
		
	}
}
