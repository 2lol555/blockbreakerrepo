﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour {
	//public static int breakableCount = 0;
	public AudioClip crack;
	public Sprite[] hitSprites;
	public static int breakableCount = 0;
	public GameObject smoke;
	
	private bool isBreakable;
	private int timesHit;
	private LevelManager levelmanager;
	void Start () {
		isBreakable = (this.tag == "Breakable");
		
		//keeping track of how many breakable bricks we have
		
		if (isBreakable){
			breakableCount++;
			print(breakableCount);
		}
		
		levelmanager = GameObject.FindObjectOfType<LevelManager>();
		timesHit = 0;
	}
	void Update () {

	}
	
	void OnCollisionEnter2D(Collision2D collider){
		//Play the crack sound even after the brick has been destroyed
		if(isBreakable){
			AudioSource.PlayClipAtPoint (crack, transform.position);
			HandleHits();
			
		}
	}
	void HandleHits(){
	
		int maxHits;
	
		maxHits = hitSprites.Length + 1;
		timesHit = timesHit + 1;
		
		if(timesHit >= maxHits){
			
			breakableCount = breakableCount - 1;
			
			levelmanager.brickDestroyed();
			
			print(breakableCount);
			
			puffSmoke();
			
			GameObject.Destroy(gameObject);
		} else{
			LoadSprites();
			
		}

	
	}
	void puffSmoke(){
		//This line makes a local variable of the new smokepuff with the name smoke, on the current brick pos. and it returns it as a gameobject
		GameObject smokepuff = Instantiate(smoke, transform.position, Quaternion.identity) as GameObject;
		
		//This line makes the puff the color of the current brick
		smokepuff.GetComponent<ParticleSystem>().startColor = gameObject.GetComponent<SpriteRenderer>().color;
	}
	
	void LoadSprites(){
		int spriteIndex = timesHit - 1;
		if(hitSprites[spriteIndex]){
			this.GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];	
		}else{
			Debug.LogError("Brick sprite missing!");
		}
	
	}



}
