﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {
	static MusicPlayer instance; 
	// Use this for initialization
	void Start () {
		if(instance == null){
			GameObject.DontDestroyOnLoad(gameObject);
			instance = this;
			GetComponent<AudioSource>().Play();
		} else {
			Destroy(gameObject);
		}
	}
}